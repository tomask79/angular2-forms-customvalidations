"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var product_service_1 = require('./product.service');
var product_detail_component_1 = require('./product.detail.component');
var product_form_component_1 = require('./product-form.component');
var AppComponent = (function () {
    function AppComponent(productService) {
        this.productService = productService;
        this._boughtProducts = [];
    }
    AppComponent.prototype.ngOnInit = function () {
        this._products = this.productService.getProducts();
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "<h1>Working with FormBuilder</h1>\n            <div>\n                <product-form [(productList)] = \"_products\"></product-form>\n            </div>\n            <hr>\n            <ul>\n                <li *ngFor=\"#product of _products\">\n                    <product-detail [product]=\"product\"></product-detail>\n                </li>\n            </ul>           \n    ",
            providers: [product_service_1.ProductService],
            directives: [product_detail_component_1.ProductDetailComponent, product_form_component_1.ProductFormComponent]
        }), 
        __metadata('design:paramtypes', [product_service_1.ProductService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map