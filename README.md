# Writing applications in Angular 2, [part 9] #

## Short summary regarding forms in Angular 2 ##

Since this is my last post regarding forms in Angular2, let's summarize things a little bit. 
When writing forms in Angular 2, basically there two approaches:

* ** Template driven ** Using *ngForm directive* with static ngControls in HTML added into ControlGroup exposed by ngForm directive. See my demo, [part 7](https://bitbucket.org/tomask79/angular2-forms-intro).
* ** Model driven ** - Usually done via FormBuilder where you're adding controls into your manually created control group. Validations is possible to add dynamically.
See my demo, [part 8](https://bitbucket.org/tomask79/angular2-forms-formbuilder)

## Howto create custom validation in Angular 2 ##

Okay, into my product form, let's **add new custom validation checking whether user has entered price of product bigger then zero.**
First we're gonna create class containing our validation:

```
import {AbstractControl} from '@angular/forms';

export class CustomValidators {
  static priceValueValidation(control: AbstractControl): { [s: string]: boolean } {
    // Validation is OK only if control is not changed or has number bigger > 0 otherwise throw error.
    return (control.pristine || parseInt(control.value)) > 0 ? null : {"lessThenZeroPriceError": true};
  }
}
```
Price control is going to be injected by Angular 2. If entered price is not bigger then zero then we're returning error "lessThenZeroPriceError".
**If control has correct value then Angular 2 expects us to be returning null.** Custom validator should be just simple function in the form:


```
interface Validator<T extends Control> {
   (c:T): {[error: string]:any};
}
```

Now let's add our new validation to price control, 
here is my complete ProductForm component** with new custom validation**:


```
import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ProductType} from './product';
import {CORE_DIRECTIVES, 
        FORM_DIRECTIVES, 
        FormBuilder, 
        ControlGroup, 
        Validators, 
        AbstractControl} from '@angular/common';
import {CustomValidators} from './custom.validations';

@Component ({
    selector: 'product-form',
    template: `
           <h1>Enter new product details</h1>
           <form [ngFormModel]="myForm" (ngSubmit)="onSubmit(myForm.value)">

                <div>Enter name: <input type="text"  [ngFormControl]="name"/></div>
                <div *ngIf="!myForm.pristine && name.hasError('required')"><span style="color:red">Please enter the product name!</span></div>  

                <div>Enter price: <input type="text" [ngFormControl]="price"/></div>
                <div *ngIf="!myForm.pristine && price.hasError('required')"><span style="color:red">Please enter the product price!</span></div>  
                <div *ngIf="price.hasError('lessThenZeroPriceError')"><span style="color:red">Price cannot be lower then zero!</span></div>

                <div>Enter image: <input type="text" [ngFormControl]="image"/></div> 
                <div *ngIf="!myForm.pristine && image.hasError('required')"><span style="color:red">Please enter the product image!</span></div>  

                <div *ngIf="myForm.valid"><button type="submit">CREATE</button></div>
           </form> 
    `,
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class ProductFormComponent {
    @Input() productList: ProductType[];
    @Output() productListChange = new EventEmitter<ProductType[]>();

    myForm: ControlGroup;

    name: AbstractControl;
    price: AbstractControl;
    image: AbstractControl;

    constructor(fb: FormBuilder) {
        this.myForm = fb.group({
            name: ['', Validators.required],
            price: ['', Validators.compose([Validators.required, CustomValidators.priceValueValidation])],
            image: ['', Validators.required]
        });

        this.name = this.myForm.controls['name'];
        this.price = this.myForm.controls['price'];
        this.image = this.myForm.controls['image'];
    }

    onSubmit(submitedValue: ProductType) {
        this.productList.push(submitedValue);
        this.productListChange.emit(this.productList);
    }
}
```

By code:
```
price: ['', Validators.compose([Validators.required, CustomValidators.priceValueValidation])],
```
we're saying: "Price is valid only **if it's filled and bigger then zero**"

We also need to display our new error message if our new custom validation discovers the invalid price:

```
<div *ngIf="price.hasError('lessThenZeroPriceError')"><span style="color:red">Price cannot be lower then zero!</span></div>
```


## Testing the demo ##

* git clone <this repo>
* npm install
* npm start
* visit http://localhost:3000

regards

Tomas



 